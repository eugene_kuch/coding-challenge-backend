import time

import requests

import bottle
from bottle import get, request, response
from bson import json_util
from pymongo import MongoClient


GITHUB_REPOS_ENDPOINT = 'https://api.github.com/search/repositories?q=language:{}&stars:{}&sort=stars&order=desc&per_page=100'
REPOS_FIELDS = ['full_name', 'html_url', 'description', 'stargazers_count', 'language']
client = MongoClient('mongodb://localhost:27017/')
db = client['task']


def _paginate(cursor, page, per_page=20):
    """Return one page of results with given page and cursor"""
    try:
        page = int(page)
    except TypeError:
        page = 1
    if page < 1:
        page = 1

    offset = (page - 1) * per_page
    return cursor.skip(offset).limit(page)


@get('/repositories')
def list_repositories():
    page = request.query.get('page', 1)
    repos = db.repos.find(projection={'_id': False}).sort('stargazers_count')
    response.headers['Content-Type'] = 'application/json'
    return json_util.dumps(_paginate(repos, page))


def load_github_data(language, stars_condition):
    """Load data from github and save it to the db"""
    url = GITHUB_REPOS_ENDPOINT.format(language, stars_condition)
    new_repos = []
    page = 1
    while True:
        resp = requests.get(url, params={'page': page})
        raw_repos = resp.json().get('items')
        if not raw_repos:
            break
        if resp.ok:
            new_repos += map(lambda x: {field: x[field] for field in REPOS_FIELDS}, raw_repos)
            page += 1
        elif resp.status_code == 403:
            # rate limit allows us to make up to 10 requests per minute, chill out
            time.sleep(60)
        else:
            resp.raise_for_status()
    db.repos.insert_many(new_repos)


if __name__ == '__main__':
    db.repos.remove()
    load_github_data('python', '>=500')
    bottle.run(host='127.0.0.1', port=8000)
