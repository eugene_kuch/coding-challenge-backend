# Simple appliaction based on aiohttp and mongodb
Server `app/server.py`
Github data fetcher `app/get_data.py`
Log file `log/server.log`


## How to run
1. Clone this repository
2. Run `sudo docker-compose build`
3. Run `sudo docker-compose up`
4. Run 'sudo docker-compose run app python get_data.py' to fetch data from github


## Get data
By default soring by starts ascending
- get first page: `curl 127.0.0.1:8080/data`
- get 10th page: `curl 127.0.0.1:8080/data?page=10`
- get first page sorting by stars descening: `curl 127.0.0.1:8080/data?ordering=desc`


## Manipulate data
- add one item: 
`curl -X POST -H "Content-Type: application/json" -d '{"id":1, "stargazers_count":0}' 127.0.0.1:8080/data/new`
response:
{"description": null, "stargazers_count": 0, "full_name": null, "id": 1, "html_url": null}%

- get one item: `curl 127.0.0.1:8080/data/1`
response:
`{"description": null, "stargazers_count": 0, "full_name": null, "id": 1, "html_url": null}`

- update one item:
`curl -X PUT -H "Content-Type: application/json" -d '{"description":"some text", "full_name": "thats me"}' 127.0.0.1:8080/data/1`
response:
{"description": "some text", "id": 1, "stargazers_count": 0, "full_name": "thats me", "html_url": null}

- remove one item:
`curl -X "DELETE" 127.0.0.1:8080/data/1`
response:
`OK`

## Misc
- test error logging
`curl 127.0.0.1:8080/data/error`
response:
500: Internal Server Error

## TODOS
- Add more logging
- Protect operations with auth header and token
- Protect mongodb with usernme and password
- Make pagination 
- Use ORM instead of cutom functions in `db.py` module
- Add settings as envvars
- Add generic validators
