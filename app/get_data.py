import asyncio
from aiohttp import ClientSession
from datetime import datetime
import time
from db import create_empty_db, insert_bulk
from log import logger


GIT_URL_PAGINATION = 'https://api.github.com/search/repositories?q=stars:>=300+language:python&per_page={}&page={}'
PER_PAGE = 100
TOTAL_NUM = 1000

# INFORMATION!
# Actually this function should be used instead of get_data()
# The reason is that github requires to fetch search result page by page
# and the links to the next/previous pages are in the response header.
# But in this case we cannot fetch all pages at once, so let's break the law.
#
# GIT_URL = 'https://api.github.com/search/repositories?q=stars:>=300+language:python&per_page=50'
#
# def get_next_page(headers):
#     if 'Link' in headers:
#         page_links = headers['Link'].split(", ")
#         for page_link in page_links:
#             (url, rel) = page_link.split("; ")
#             if rel == 'rel="next"':
#                 return url[1:-1]
#     return None
#
# async def get_data_correct():
#     await empty_db()
#
#     async with ClientSession() as session:
#         page = 1
#         next_url = GIT_URL
#         while True:
#             async with session.get(next_url) as response:
#                 if response.status == 200:
#                     data = await response.json()
#                     await insert_bulk(data)
#
#                     next_url = get_next_page(response.headers)
#                     if not next_url:
#                         return
#                     page += 1
#                 elif response.status == 403:
#                     now = int(time.mktime(datetime.now().timetuple()))
#                     if response.headers.get('X-RateLimit-Reset'):
#                         wait_until = int(response.headers['X-RateLimit-Reset'])
#
#                         time_to_sleep = wait_until - now + 1
#                     elif response.headers.get('Retry-After'):
#                         time_to_sleep = int(response.headers.get('Retry-After')) + 1
#                     else:
#                         # failed to get data
#                         return
#                     await asyncio.sleep(time_to_sleep)
#                     continue
#                 else:
#                     # failed to get data for unknown reason
#                     print('wtf resp', response.status)
#                     return


# This function gets repositories list from github and inserts to db
# Known limitations (https://developer.github.com/v3/search/):
# - the GitHub Search API provides up to 1,000 results for each search
# - For unauthenticated requests, the rate limit allows you to make up to 10 requests per minute.
# - This method returns up to 100 results per page.
async def get_data():
    await create_empty_db()

    tasks = []
    async with ClientSession() as session:
        for page in range(1, TOTAL_NUM // PER_PAGE + 1):
            task = asyncio.ensure_future(fetch(page, session))
            tasks.append(task)
        await asyncio.gather(*tasks)


async def fetch(page, session):
    url = GIT_URL_PAGINATION.format(PER_PAGE, page)
    while True:
        async with session.get(url) as response:
            if response.status == 200:
                logger.info('got data for page {}'.format(page))
                data = await response.json()
                await insert_bulk(data)
                return
            elif response.status == 403:
                now = int(time.mktime(datetime.now().timetuple()))
                if response.headers.get('X-RateLimit-Reset'):
                    wait_until = int(response.headers['X-RateLimit-Reset'])
                    time_to_sleep = wait_until - now + 1
                elif response.headers.get('Retry-After'):
                    time_to_sleep = int(response.headers.get('Retry-After')) + 1
                else:
                    # failed to get data
                    logger.error('could not get data from github, rate limits headers are not set. response status: {}. headers: {}'.format(response.status, response.headers))
                    return
                logger.info('could not get data for page {} - sleep {} sec'.format(page, time_to_sleep))
                await asyncio.sleep(time_to_sleep)
            else:
                # failed to get data for unknown reason
                logger.error('could not get data from github for unknown reason. response status: {}. headers: {}'.format(response.status, response.headers))
                return


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    future = asyncio.ensure_future(get_data())
    loop.run_until_complete(future)
    loop.close()
