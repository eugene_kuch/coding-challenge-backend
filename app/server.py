from aiohttp import web
from db import db_fetch_item, db_fetch_items, db_delete_item, db_update_item, db_create_item
from settings import PER_PAGE
from validators import get_page, get_sort_direction, validate_item_id, validate_data
from exceptions import AppException, AppValidationException
import functools
from log import logger


# log automatically uncaught exceptions
def auto_log():
    def wrapper(func):
        @functools.wraps(func)
        async def wrapped(*args, **kwargs):
            try:
                return await func(*args, **kwargs)
            except web.HTTPException:
                raise
            except Exception as ex:
                logger.error(ex, exc_info=True)
                raise web.HTTPInternalServerError()
        return wrapped
    return wrapper


@auto_log()
async def list_items(request):
    """
    return list of items with pagination and sort
    """
    page_num = get_page(request.GET.get('page'))
    sort_direction = get_sort_direction(request.GET.get('ordering'))

    try:
        data = await db_fetch_items(PER_PAGE, page_num, sort_direction)
    except AppException as ex:
        raise web.HTTPNotFound(text=str(ex))

    return web.json_response(data)


@auto_log()
async def get_item(request):
    """
    return item by id or 404
    """
    try:
        item_id = validate_item_id(request.match_info.get('id'))
    except AppValidationException as ex:
        raise web.HTTPNotFound(text=str(ex))

    try:
        data = await db_fetch_item(item_id)
    except AppException as ex:
        raise web.HTTPNotFound(text=str(ex))

    return web.json_response(data)


@auto_log()
async def create_item(request):
    """
    create item. required fields:
        'id' - unique int
        'stargazers_count' - non-negative int
    """
    raw_data = await request.json()
    try:
        data = validate_data(raw_data, required_fields=True)
    except AppValidationException as ex:
        raise web.HTTPBadRequest(text=str(ex))

    try:
        await db_create_item(data)
    except AppException as ex:
        raise web.HTTPBadRequest(text=str(ex))

    return web.json_response(data)


@auto_log()
async def delete_item(request):
    """
    remove item by id or 404
    """
    try:
        item_id = validate_item_id(request.match_info.get('id'))
    except AppValidationException as ex:
        raise web.HTTPNotFound(text=str(ex))

    try:
        await db_delete_item(item_id)
    except AppException as ex:
        raise web.HTTPNotFound(text=str(ex))

    return web.Response(text='OK')


@auto_log()
async def update_item(request):
    """
    update item by id or 404
    fields which are not provided will not be updated
    """
    try:
        item_id = validate_item_id(request.match_info.get('id'))
    except AppValidationException as ex:
        raise web.HTTPNotFound(text=str(ex))

    raw_data = await request.json()
    raw_data['id'] = item_id

    try:
        data = validate_data(raw_data)
    except AppValidationException as ex:
        raise web.HTTPNotFound(text=str(ex))
    raw_data.pop('id')
    try:
        data = await db_update_item(item_id, data)
    except AppException as ex:
        raise web.HTTPBadRequest(text=str(ex))

    return web.json_response(data)


@auto_log()
async def test_error(request):
    """
    example of handling and logging exceptions
    """
    _ = 1 / 0
    return web.Response(text='OK')


my_web_app = web.Application()
my_web_app.router.add_route('GET', '/data', list_items)
my_web_app.router.add_route('POST', '/data/new', create_item)
my_web_app.router.add_route('GET', '/data/{id:\d+}', get_item)
my_web_app.router.add_route('DELETE', '/data/{id:\d+}', delete_item)
my_web_app.router.add_route('PUT', '/data/{id:\d+}', update_item)
my_web_app.router.add_route('GET', '/data/error', test_error)

logger.info('server started')
