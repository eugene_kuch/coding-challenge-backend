from pymongo import ASCENDING, DESCENDING
from settings import FIELDS
from exceptions import AppValidationException


def as_int(raw_item_id):
    try:
        item_id = int(raw_item_id)
    except (ValueError, TypeError):
        return None
    if item_id >= 0:
        return item_id
    return None


def get_page(raw_page):
    if raw_page:
        try:
            page_num = int(raw_page)
        except (ValueError, TypeError):
            return 1
        if page_num > 0:
            return page_num
    return 1


def get_sort_direction(raw_ordering):
    if raw_ordering == 'desc':
        return DESCENDING
    return ASCENDING


def validate_item_id(raw_item_id):
    item_id = as_int(raw_item_id)
    if not item_id:
        raise AppValidationException('item id {} incorrect'.format(raw_item_id))
    return item_id


def validate_field(field, raw_value):
    value = raw_value
    validator = VALIDATORS.get(field)
    if validator:
        value = validator(raw_value)
    return value


def validate_data(raw_data, required_fields=False):
    data = {}
    for field in FIELDS:
        value = validate_field(field, raw_data.get(field))
        if value is None:
            continue
        data[field] = validate_field(field, raw_data.get(field))

    if required_fields:
        if 'id' not in data:
            raise AppValidationException('"id" field is required')
        if 'stargazers_count' not in data:
            raise AppValidationException('"stargazers_count" field is required')
    return data


VALIDATORS = {
    'id': as_int,
    'stargazers_count': as_int, 
}
