import os
import motor.motor_asyncio
from settings import FIELDS
from exceptions import AppException
from pymongo.errors import DuplicateKeyError
from pymongo import ASCENDING, DESCENDING


client = motor.motor_asyncio.AsyncIOMotorClient('mongodb://mongodb:27017')
db = client.github
repositories = db.repositories


def clean_row(row):
    return {field: value for field, value in row.items() if field in FIELDS}


def clean_data(data):
    return [clean_row(row) for row in data['items']]


async def insert_bulk(data):
    data = clean_data(data)
    await db.repositories.insert(data)


async def create_empty_db():
    global db
    await client.drop_database('github')
    db = client['github']
    db_repositories = db['repositories']
    db_repositories.create_index([("id", ASCENDING)], unique=True)
    db_repositories.create_index([("stargazers_count", ASCENDING)])
    db_repositories.create_index([("stargazers_count", DESCENDING)])



async def db_fetch_items(per_page, page_num, sort_direction):
    cursor = db.repositories.find({}, {'_id': 0 }).sort('stargazers_count', sort_direction).skip(per_page * (page_num - 1)).limit(per_page)
    data = await cursor.to_list(per_page)
    if not data:
        raise AppException('no data found')
    return data


async def db_fetch_item(item_id):
    data = await db.repositories.find_one({'id': item_id}, {'_id': 0 })
    if not data:
        raise AppException('item #{} not found'.format(item_id))
    return data


async def db_delete_item(item_id):
    res = await db.repositories.delete_many({'id': item_id})
    if not res.deleted_count:
        raise AppException('item #{} not found'.format(item_id))


async def db_update_item(item_id, data):
    res = await db.repositories.update_one({'id': item_id}, {'$set': data})
    if not res.matched_count:
        raise AppException('item #{} not found'.format(item_id))
    data = await db_fetch_item(item_id)
    return data


async def db_create_item(data):
    try:
        await db.repositories.insert_one(data)
    except DuplicateKeyError:
        raise AppException('item #{} already exists'.format(data['id']))
        
    data.pop('_id')
    return data
