class AppException(Exception):
    pass


class AppValidationException(Exception):
    pass
