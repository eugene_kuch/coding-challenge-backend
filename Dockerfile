FROM python:3.5

#RUN mkdir /code
RUN mkdir /log
RUN chmod -R 0777 /log
ADD ./app /app/
WORKDIR /app

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

#EXPOSE 8080

#ENTRYPOINT ['gunicorn', 'server:my_web_app', '--bind', 'localhost:8080' '--worker-class aiohttp.worker.GunicornWebWorke']
